<?php
/*
 * Adventskalender.php - a plugin for Stud.IP
 * Copyright (c) 2015 Thomas Stieglmaier
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of Apache License, Version 2.0.
 */

class Adventskalender extends StudipPlugin implements PortalPlugin
{
    /**
     * Return a template (an instance of the Flexi_Template class)
     * to be rendered on the start or portal page. Return NULL to
     * render nothing for this plugin.
     */
    public function getPortalTemplate()
    {
        $template_path = $this->getPluginPath().'/templates';
        $template_factory = new Flexi_TemplateFactory($template_path);
        $template = $template_factory->open('Adventskalender');

        $template->title = $this->getPluginName();
        $template->icon_url = $this->getPluginURL() . '/images/icon.gif';
	$template->advent_url = "https://ieee.uni-passau.de/advent#doors";

        return $template;
    }
}
?>
